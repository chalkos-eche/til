## 클래스 컴포넌트에서의 constructor 와 super()

### constructor(props) 
* 컴포넌트가 생성할때 state값을 초기화 하거나 메서드를 바인딩 할 때 사용
* 해당 컴포넌트가 마운트 되기전에 호출됨.

### super(props)
* super는 부모 컴포넌트의 생성자를 참조함
* super() 를 선언 전 까진 constructor() 안에 this키워드를 사용 할 수 없음. 
