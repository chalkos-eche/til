# SPA
* Single Page Apllication
* 단일 웹페이지로 돌아가는 애플리케이션
* 검색엔진 최적화에 적합하지 않다.
* REACT,SVELTE,VUEJS 같은 라이브러리로 개발 가능

## 리액트 라우터
* 개발자가 주소별로 다른 컴포넌트를 보여주기 위해 사용하는 라이브러리
* 여러 환경에서 동작할 수 있도록 여러 종류의 라우터 컴포넌트 제공

### BrowserRouter
* <BrowserRouter>
* html5 를 지원하는 브라우저 감지

### Routes,Route
* 경로가 일치하는 컴포넌트를 렌더링해주도록 경로 매칭
* Route에서는 구체적으로 어떤 컴포넌트를 렌더링할지 결정
* Route path = 경로 , element = 연겷한 컴포넌트

### Link 
* 경로를 변경한다.
* a태그와 같음
