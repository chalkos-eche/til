// 콜백지옥을 벗어나는 함수

const delayedColorChange = (newColor, delay) => {
  setTimeout(() => {
    document.body.style.backgroundColor = newColor;
  }, delay);
};
delayedColorChange("olive", 1000);
delayedColorChange("teal", 1000); // ?
