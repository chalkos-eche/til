import ReactDOM from 'react-dom/client';

import './index.css'; // 웹페이지 공통의 layout.css 불러오기
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
// reactDom 에서 createRoot 을 호출함 (역할 : 메인 엔트리 포인트 // 메인 훅 이라고도 함 )
// React로 만들 UI 가 어디에 로딩되어야하는지 위치를 정해줌. ( public / index.html )
// eslint-disable-next-line react/react-in-jsx-scope
root.render(<App />); // JSX 컴포넌트를 불러옴 div#root 에 App jsx를 구현함.

// 리액트의 제일 먼저 실행되는 영역
// react-dom 객체 : REACT 와 함께 이루는 의존성 라이브러리
