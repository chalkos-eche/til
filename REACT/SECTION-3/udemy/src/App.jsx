import React from "react";
// 새로만든 컴포넌트는 import 해줘야 루트 컴포넌트에 중첩 할 수 있다.
// import ExpenseItem from "./components/ExpenseItem";
import Expenses from "./components/Expenses";

function App() {
  const expenses = [
    {
      id: "e1",
      title: "Toilet Paper",
      amount: 94.12,
      date: new Date(2020, 7, 14),
    },
    {
      id: "e2",
      title: "New TV",
      amount: 799.49,
      date: new Date(2021, 2, 12),
    },
    {
      id: "e3",
      title: "Car Insurance",
      amount: 294.67,
      date: new Date(2021, 2, 28),
    },
    {
      id: "e4",
      title: "New Desk (Wooden)",
      amount: 450,
      date: new Date(2021, 5, 12),
    },
  ];
  // // 또는 미리 새로운 요소를 생성해야함 createElement 같이
  // const para = document.createElement(" p");
  // para.textContent = "This is also visible !";
  // // 그 후 app end 를 이용하여 본문에 삽입한다
  // document.getElementById("root").append(para);
  // // 이러한 작업이 매우 귀찮으니 return 문 밑에 자동으로 생성하게한다
  // // 컴포넌트의 기본 개념
  return (
    <>
      <h2>Let's get started!</h2>
      {/* jsx 태그에 props 를 보낼 수 있다 */}
      <Expenses expenses={expenses} />
      <ExpenseItem
        title={expenses[0].title}
        amount={expenses[0].amount}
        date={expenses[0].date}
        id={expenses[0].id}
      />
      <ExpenseItem
        title={expenses[1].title}
        amount={expenses[1].amount}
        date={expenses[1].date}
        id={expenses[1].id}
      />
      <ExpenseItem
        title={expenses[2].title}
        amount={expenses[2].amount}
        date={expenses[2].date}
        id={expenses[2].id}
      />
      <ExpenseItem
        title={expenses[3].title}
        amount={expenses[3].amount}
        date={expenses[3].date}
        id={expenses[3].id}
      />
    </>
    // 일반 js 에선 페이지에서 어떤 요소를 선택 하려면
    // querySelector 같은거 사용했으나
    // react 에서는 innerHTML 을 설정해야함,
  );
}

export default App;

// App 컴포넌트는 모든 컴포넌트의 루트 컴포넌트가 될것 다른곳에 중첩됨.
// 컴포넌트 트리 최상단

// 리액트는 Props 라는 기능으로 부모 컴포넌트의 데이터를 자식 컴포넌트의 데이터로 보낼 수 있다.
