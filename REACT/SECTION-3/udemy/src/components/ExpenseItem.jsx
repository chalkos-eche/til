// 컴포넌트 파일 이름은 첫글자는 대문자로 다음 단어도 대문자로 해야함
import React from 'react';
import ExpenseDate from './ExpenseDate';
import './ExpenseItem.css';
// html 스타일링을 위해 layout.css 를 사용한다.
// 같은 폴더에서 같은명의 layout.css 파일을 작성후 import 한다.

const ExpenseItem = (props) => {
  const { date, title, amount } = props;
  // 부모컴포넌트에서 보낸 props 데이터를 파라미터(한개만 적는다)로 받고
  //
  // 기능 자바스크릡트는 return 전에 삽입한다.
  // const expenseDate = new Date(2021, 2, 28);
  // const expenseTitle = "Car Insurance";
  // const expenseAmount = 294.67;
  // 변수를 html 에서 불러올땐 {자바스크립트 변수명} 으로 불러온다.

  // const month = date.toLocaleString("en-us", {
  //   month: "long",
  // });
  // const day = date.toLocaleString("en-us", {
  //   day: "2-digit",
  //   // .toLocaleString() 인간의형태로 포맷을 변경 해줌.
  // });
  // const year = date.getFullYear();
  return (
    <>
      {/* 매개변수.이름 으로 정해준다 */}
      <div className="expense-item">
        {/* {date.toISOString()} */}
        {/* date.toISOString 을 바꿀것 */}
        {/* <div> */}
        {/*  <div>{month}</div> */}
        {/*  <div>{year}</div> */}
        {/*  <div>{day}</div> */}
        {/* </div> */}
        {/*  위에 캘린더 div 를 추가 컴포넌트로 뺴줄것 */}
        <ExpenseDate date={date} />
      </div>

      <div className="expense-item__description">
        <h2>{title}</h2>
        <div className="expense-item__price">${amount}</div>
      </div>
    </>
  );
};

export default ExpenseItem;
// class 대신 className 을 사용해야한다.

// 컴포넌트의 핵심은 재사용성

// title price desc 와 같은건 동적 데이터로 변할 수 있다.
