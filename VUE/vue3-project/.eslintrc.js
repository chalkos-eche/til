module.exports = {
  root:true,
  env:{
    node:true,
  },
  extends:[
    'plugin:vue/vue3-essential',
    'plugin:vue/base',
    'plugin:vue/recommended',
    'eslint:recommended',
    'prettier',
  ],
  parser:'vue-eslint-parser',
  parserOptions:{
    parser:'babel-eslint',
    "ecmaVersion":2020,
  },
  rules:{
    "vue/no-multiple-template-root":"off",
    'no-console':
      process.env.NODE_ENV ===
      'production'
        ? 'warn'
        : 'off',
    'no-debugger':
      process.env.NODE_ENV ===
      'production'
        ? 'warn'
        : 'off',
    'vue/comment-directive':'off',
    indent:'off',

  },
};
