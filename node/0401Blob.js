//Blob 객체를 URL로 만들기


//createObjectURL
let blob = new Blob([new ArrayBuffer(data)], { type: "image/png" });

const url = window.URL.createObjectURL(blob); // blob:http://localhost:1234/28ff8746-94eb-4dbe-9d6c-2443b581dd30

//  이미지 태그를 blob 주소로 만들기.
document.getElementById("image").src = url;

// revokeObjectURL  || 메모리 최적화 (종료)

// blob 생성

// url 생성
const url2 = window.URL.createObjectURL(blob); // blob:http://localhost:1234/28ff8746-94eb-4dbe-9d6c-2443b581dd30

// url 사용 후에 메모리에서 제거하기
window.URL.revokeObjectURL(url2);
