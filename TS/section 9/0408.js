"use strict";
const thomas = {
    first: "Thomas",
    last: "Hardy",
    nickname: "Tom",
    id: 21837,
};
thomas.first = "changed name";
// thomas.id = 324; // error
