interface Todo {
  text: string;
  completed: boolean;
}

const btn = document.getElementById('btn')! as HTMLButtonElement;
const input = document.getElementById('todoInput')! as HTMLInputElement;
// const form = document.getElementById('todoForm')! as HTMLFormElement;
const form = document.querySelector('form')! as HTMLFormElement;
const list = document.getElementById('todolist')! as HTMLUListElement;

// 인터페이스 추가하기
const todos:Todo[] =[]
// {
// text:'walk the dog',
// completed :false,
// }
function handleSubmit(e:SubmitEvent) {
  //  함수를 따로빼뒀을때는 SubmitEvent 라고 명시해둬야함
  e.preventDefault();
console.log('submitted');

}
form.addEventListener('submit', function(e) {
  // 타입스크립트에서  e가 무너지 알고있는가 : SubmitEvent 라고 미리 알구있음
  e.preventDefault();
  //자바스크립트 데이터상에 입력
  const newTodo:Todo ={
    text: input.value,
    completed: false,
  }
  //화면에 출력하기
  //데이터상 입력부분과 화면에 출력하기는 다른 함수로 나누어서 표현한다
  createTodo(newTodo)
  todos.push(newTodo);
 
console.log('submitted');
});

function createTodo(todo:Todo) {
  const newLi = document.createElement('li');
  const checkbox = document.createElement('input');
  checkbox.type = 'checkbox';
  newLi.append(todo.text);
  newLi.append(checkbox);
  list?.append(newLi);
  input.value ='';

}
