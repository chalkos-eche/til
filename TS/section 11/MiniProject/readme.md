

* html 상에서 button을 만들고 
* querySelector로 button 을 선택 할 수 있다.

```js
//js 의 방식 
<button>CLICK</button>
const btn = document.querySelector('button');
``` 
* document의 타입은 무엇일까?
`console.dir(document)`  
* 브라우저의 특별한 객체로서 , 수많은 프로퍼티/메서드를 갖고 있다
타입을 설정하기전에 document안에는 다양한 string ,number, array 등  타입으로 나누기 애매하다.

```ts
btn.addEventListener() // ts 에서 btn은 addEventListener라는 메서드가 없다고 에러가 뿜음
```
* Typescript 는 문서 객체 즉 DOM 요소 및 기타 요소의 타입을 인지한다.(querySelector,addEventListener..)
* document는 document 타입이다 ...!?
* 타입스크립트 내장으로 document 타입이 지정되어 있어, 
[//]: # (<T> 는 제네릭타입)
* **document 오브ㅈ
