/*
 * typescript만의 고유한 타입 Tuples
 * 튜플/ 터플
 *
 * 개념은 본질적으로  "배열 타입"
 * but,, 고정된 "길이"를 갖는ㅂ ㅐ열
 * 고정된 타입 세트로 순서가 정렬됨
 *
 * */

let myTuple: [number, string];
myTuple = [10, "abc"];
myTuple = ["abc", 10]; //에러 뜸

const stuff: (string | number)[] = [1, "asd", "fgh", "qwe"];
// 일반 유니온 배열 타입

// 튜플은 배열의 개수와 위치가 고정 되어 있다.

const color: [number, number, number] = [255, 0, 1];
// 문자열 X , 4개 이상의 길이 X

// 예를들어 HTTP 통신을 통해 메시지를 저장하는 튜플

type HTTPResponse = [number, string];

const good: HTTPResponse = [200, "Ok"];
const bad: HTTPResponse = [200, "BAD"];
//튜플은 개별요소 변경을 허락 X
good[0] = "200"; // 숫자 -> 문자 에러

// 튜플이 설계되고 구현된 방식에서
// 튜플 생성후 추가 요소를 푸시 하는것을 막지 않음. (반직관적)

good.push(200); // 에러안뜸 + 튜플의 한계

good.pop();
good.pop();
good.pop();
//튜플이 있는데 빈배열이 됨.

// 다행스러운 점은 많이 사용할 일이 없음.
