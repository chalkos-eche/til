// Enums

// Js에는 존재하지않는 Ts의 타입

// Enums 는 다른 언어에서 사용되며

// enum의 정의 : 명명된 상수 집합을 정의할 수 있다

// 반복적으로 참조하는 값의 집합이 있을 경우에 사용됨
//숫자 에넘
import { number } from "prop-types";

enum Responses {
  no,
  yes,
  maybe,
}

enum Responses {
  no = 2,
  yes,
  maybe,
}
enum Responses {
  no = 2,
  yes = 10,
  maybe = 24,
}
//문자열 에넘

enum Responses {
  no = "No  ",
  yes = "Yes",
  maybe = "Maybe",
}

enum Responses {
  no = 0,
  yes = 1,
  maybe = "Maybe",
}

// 예)
// 화살표 키 방향
UP;
DOWN;
LEFT;
RIGHT;

// 주문 상태 코드
PENDING;
SHIPPED;
DELIEVERED;

// 에넘이 없을 때 작업방법

const PENDING = 1;
const SHIPPED = 2;
const DELIVERED = 3;

// 코드 전반에 걸쳐 숫자를 사용 할 수 있다.

if (status === DELIVERED) {
  console.log("1");
}
//DELIVERED 대신 0을 넣으면 코드가 말이 안되서
// O의 상태 VS DELIVERED의 차이
// DELIVERED라는 명명된 상수를 원하는것
// 0,1,2는 의미가 중요하지않음

//** i,e) Enum은 상수이름의 집합

// enum 이름과 ,값의 목록

enum OrderStatus {
  PENDING,
  SHIPPED,
  DELIVEREd = 23,
  RETURNED,
}

const myStatus = OrderStatus.DELIVERED;

function isDelivered(status: OrderStatus) {
  return status === myStatus;
}

isDelivered(OrderStatus.RETURNED);

// enum을 생성하고 값을 지정하지 않으면 알아서 0부터 시작 함

// 드문 방식으로 , 값을 따로 지정 할 수도 있음.

// enum을 문자열로도 가능함

enum ArrowKeys {
  UP = "up",
  DOWN = "down",
  LEFT = "left",
  RIGHT = "right",
  ERROR = 234,
}

//다 같은 타입일 이유는 없음.

if (move == ArrowKeys.LEFT)
  //
  // 백그라운드에서의 Enum
  //
  // enum은 실제로 자바스크립트에 영향을 미침 ( 추가적인 코드로 표현 됨)

  // enum을 작성하면 자바스크립트 상에서 즉시실행 함수와 객체로 이루어진다.
  let OrderStatusJS: any;
(function (OrderStatusJS) {
  OrderStatusJS[(OrderStatusJS["PENDING"] = 0)] = "PENDING";
  OrderStatusJS[(OrderStatusJS["PENDING"] = 1)] = "SHIPPED";
  OrderStatusJS[(OrderStatusJS["PENDING"] = 2)] = "DELIVERED";
  OrderStatusJS[(OrderStatusJS["PENDING"] = 3)] = "RETURNED";
})(OrderStatusJS || (OrderStatusJS = {}));

const order = {
  orderNumber: 2313123213,
  status: OrderStatus.PENDING,
};
