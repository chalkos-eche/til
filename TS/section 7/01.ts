//
// 유니온타입
//

// 변수의 타입을 단일이아닌 복수 선택이 가능하다.
let age: number = 21;
let 나이: number | string | boolean = 21;
나이 = '24';
나이 = true;

type Point = {
  x: number;
  y: number;
};

type Loc = {
  lat: number;
  long: number;
};

let 좌표: Point | Loc = {
  x: 1,
  y: 34,
};
좌표 = {
  lat: 321.123,
  long: 231.122,
};
// 커스텀 타입도 복수 사용 가능 하다.

//
// 유니온 타입으로 타입 좁히기 ( Narrowing)
//

// 함수에서 유니온 타입 사용하기

function 나이프린트(age: number | string): void {
  console.log(`${age}}`);
}

나이프린트(23);
나이프린트('23');

function 세금계산(price: number | string, tax: number) {
  return price * tax;
}

// price 오류  문자*숫자가 될 수 도있어서.

function 세금계산2(price: number | string, tax: number) {
  if (typeof price === 'string') {
    // price = price.replace('$', '');
    price = parseFloat(price.replace('$', ''));
  }
  return price * tax;
}
//replace는 number에 못해서 오류뜸
// $가없다고 판단

//
//
// 유니온 타입과 배열
//

//number와 string 있는 배열
const stuff: (number | string)[] = [1, 2, 3, '4'];
const sutff2: number[] | string[] = [1, 2, '3'];
//서로 의미가 다르다. 밑에는 오류가 뜸
// 위: 숫자나 문자 배열
// 밑: 숫자배열이거나 문자배열이거나

const coords: (Point | Loc)[] = [];
coords.push({ lat: 321.213, long: 23.334 });
coords.push({ x: 213, y: 31 });

//
// 리터럴 타입 ( Literal Type)
//

// 유니온타입과 항상 사용함

let zero: 0 = 0;
// 아무숫자나 넣을수있는 number가 아닌,리터럴값인 0 만 가능하다.
zero = 2; //에러 가 뜸

let hi: 'hi' = 'hi';

//유니온 타입에 리터럴 타입 활용은 유용함

const 대답 = (answer: '네' | '아니오' | '몰루') => {
  return `네 대답은 ${answer}`;
};
대답('네') O
대답('ㄴㄴ')X

let 분위기:"happy"|"sad" = "happy";
분위기 ="sad";

type DayOfWeek = "월"|"화"|"수"|"목"|"금"|"토"|"일"

let 오늘:DayOfWeek = "토"
