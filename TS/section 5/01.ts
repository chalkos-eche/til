//
// 객체 타입 다루기
//

const 개 = {
  이름: '멍멍이',
  종: '슈나우저',
  나이: 1,
};

// 두가지방법
// 1. 변수 자체
// 2. 함수에

function 이름(person: { first: string; last: string }): void {
  console.log(`${person.first} ${person.last}`);
}

이름({ first: '토마스', last: '뮐러' });

//
// 객체타입 더 알아보기
//

//
let 좌표: { x: number; y: number } = { x: 34, y: 34 };

//함수의 반환 상태도 애너테이션 할 수 있다.

function 무작위좌표(): { x: number; y: number } {
 // return {}; //빈 객체를 반환하면 오류가 생김
  return { x: Math.random(), y: Math.random() };
}

//
//초과 프로퍼티
//

// 함수의 객체 파라미터가 이해가 잘 안될 수 있음

이름({ first: '나', last: '상실' });

//** 여기에 다른 프로퍼티를 추가하게 된다면 ?

이름({ first: '나', last: '상실', age: 25 });
//오류가 뜸

//** 객체를 선언한다면?
const 나상실 = { first: '나', last: '상실', age: 25, isAlive: true };
//이 변수를 함수에 넣는다면
이름(나상실); //오류가 없음

//필요한 프로퍼티가 전달되었는지 안되었는지만 확인하고 넘어감 (나머지 무시)

// 직접 객체 리터럴은 오류가 뜨지만, 변수로 정의하는 과정을 거치면 나머지 프로퍼티는 무시 하게 됨.

//27부터

//0302

//
// 타입 별칭 (type alias)
//

// 여러 프로퍼티를 가지는 "객체 타입" 같은 복잡한 타입에 이런 작업을 함.

// 길어진 타입을 온갖곳에 사용하면 가독성 다운

// 타입을 따로 빼준다.

function 이중점(point: { x: number; y: number }): {
  x: number;
  y: number;
} {
  return { x: point.x * 2, y: point.y * 2 };
}

// 같은걸 반복 작성 하는대신 하나로 모아서 관리해줌

//** 하나로 묶을때 type 키워드로 사용한다.

//예시
type Point = {
  x: number;
  y: number;
};

//타입에 그냥 이름만 붙였다 생각하면됨 (관리하기용)
// 그럼 변수명 쓰듯이 타입명으로 사용가능

//예시
let 좌표4: Point = { x: 255, y: 255 };

function 랜덤좌표(): Point {
  return { x: Math.random(), y: Math.random() };
}

function 좌표두배(point: Point): Point {
  return {
    x: point.x * 2,
    y: point.y * 2,
  };
}

// type 은 객체 뿐만 아니라, 원시타입(문자 숫자) 등도 만들 수 있다

type phone = number;

//
// 중첩 객체 (Nested objects)
//

// 중첩 객체는 type 으로 보기좋게 관리 할 수 있다.

const 사람묘사 = (person: {
  name: string;
  age: number;
  parentNames: {
    dad: string;
    mom: string;
  };
}) => {
  return `
  Person:${name},
  Age:${age},
  parents:${parentNames.dad},${parentNames.mom}
  `;
};
사람묘사({ name: '이', age: 10, parent: { parentNames.mom: '누구', {parentNames.dad: '아빠' } })};



  type Song ={
    title:string,
    artist:string,
    numStreams:number,
    credits:{
      producer:string,
      writer:string}
  }
function calculatedPayout(song:Song) :number{
              return  song.numStreams*0.0033
}


function PrintSong(song:Song):void {
  console.log(`${song.title}- ${song.artist}}`);

}
const mySong:Song= {
title :"123",
    artist:"456",
    numStreams:789,
    credits:{
  producer:"321",
      writer:"654"

}
}

const earnings =  calculatedPayout(mySong);
console.log(earnings);
  PrintSong(mySong);


//
// 선택적 프로퍼티
//

// 일부 프로퍼티를 선택적요소로 생성 할 수 있다

type 포인트 = {
  x:number;
  y:number;
  z?: number; // 선택적 프로퍼티, 있어도 그만 없어도 그만
}

//
//readonly 제어자
//

//객체의 프로퍼티를 readonly 로 할 경우 변경 금지

//예

type User ={
  readonly id:number;
  username:string;
}

let 유저:User ={
  id:12345,
  username:'이름',
}
유저.id =1233

//type 에서 readonly 설정시, 재할당이 불가능함.

//단, readonly 항목이 객체일 경우, 그 객체는 변경,갱신,추가가 가능하다.



//
// 교차타입
//

//교차타입은 여러타입을 & 로 결합한다.


type Circle = {
  radius:number;

}
type Colorful = {
  color:string;
}

// 교차타입
type ColorfulCircle = Circle & Colorful;


const 웃는얼굴:ColorfulCircle = {
  radius:4,
  color:"yellow",
}

type Cat ={
  numLives:number
}
type Dog = {
  breed:string
}

// 교차타입에 새타입을 추가하는 법
type CatDog = Cat &
    Dog & {
      age:number; };

const 캣독:CatDog ={
  numLives:9,
  breed:"똥개",
  age:5
}
