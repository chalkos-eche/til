//
// 문자 타입 //
//

// 코드 어디에서 타입을 작성 하는가 ?

// 기본적으로 여러가지 상황이 존재 함 : return이 문자열, 배열, 불리언 일 수 있다.

// 어떤 함수는 2개의 "숫자"파라미터를 받고, 한개의 숫자 return을 해주기도 함.

// 패턴이나 객체의 형태도 특정 할 수 있음.
// ex) color란 배열의 string 문자만 입력함.

/* Typescript의 타입을 변수로 지정 할 수 있다.*/

// 타입 애너테이션 ( 타입 선언 )
// 직관적임, 변수 이름 뒤에 :(콜론)을 적고 타입을 적어주면 됨.

const 변수명: string = "0001";
// 아스에서 이변수가 문자열이라는걸 알려주고 변수의 숫자는 스트링인걸 표기 함

// let 변수:type = value 로 작성 하면 됨.
// 타입은 숫자,불리언,문자,배열 등 원하는 대로 사용 가능
// 타입은 소문자여야함.

let 영화제목: string = "앤트맨";

//변수명위에 커서를 올리면 IDE가 어떤 타입을 가졌는지 알려줌;
영화제목 = "ET";
//IDE에서 타입이 틀렸다고 오류를 뿜음
// 영화제목 = 13;
//변수에 문자가 존재하지않아 upper에서 오류가 뜨는 모습
// 영화제목.upper();
영화제목.toUpperCase();

//
// 숫자와 불리언 타입 //
//

// 정수형 / 부동수형 나누지 않음

let 불리언: boolean = true;
let 숫자: number = 1541;

let 고양이목숨: number = 9;
고양이목숨 += 1;

let 게임오버: boolean = false;
게임오버 = true;

//타입 애너테이션은 타입과 구문의 기초를 배울 때는 좋은 도구이지만,
// 실제로 변수를 작성할때는 타입 추론(Inference)를 사용한다.

let x = 27;
x = "숫자"; // 타입 추론으로인해 x의 타입을 숫자로 인식

//타입 추론

let 드라마 = "지옥";
// 변수명 드라마에 마우스 커서를 위치하면 string이라고 알려줌
드라마 = "수리남"; // 작동 OK
드라마 = false; // 에러 작동

let 재밌음 = false;
재밌음 = true;
재밌음 = "sㄴ"; // 오류

// 이런식으로 추론을 사용 할 수 있지만, 의도적으로 애너테이션을 할당 하기도 함.

//
// Any타입 //
//

// 일반적으로 잘 사용하지 않지만, 알구 있어야함

let 어떤것: any = "";
어떤것 = 1541;
어떤것 = true;
어떤것();
어떤것.toUpperCase();

//
// 지연된 초기화 및 암묵적 Any
//

const 영화 = ["아바타2", "앤트맨", "써치2", "슬램덩크"];

let 영화찾기; //정의되지않은 변수

for (let 영화 of 영화) {
  if (영화 === "슬램덩크") {
    영화찾기 = 영화;
  }
}

//any타입 할당은 피할것 : 타입스크립트의 역할을 하지 못함
