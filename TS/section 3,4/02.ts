//
// 함수 파라미터 애너테이션
//

// 암시적 타입 변환(implicit typing) 때문에 흔히 사용하지 않음.

//타입을 통해 함수 파라미터와 반환값에 할당하는 법

function 사각형(num) {
  num.toUpperCase();
  return num * num;
}
//지금 여기서 파라미터는 값이 any임.
//파라미터도 타입을 정해줘야함

function 사각형1(num: number) {
  return num * num;
}

사각형1(3);

function 인사(person: string) {
  //person은 ts에서 string으로 알구잇음.
  person * person; // 문자열 곱셈은 오류
  return `Hi,${person}!`;
}

인사(true); //타입 오류

//ts에서 파라미터가 너무부족하거나 많으면, 에러를 띄움

//
// 기본 파라미터를 이용해 작업하기
//

function 인사2(person: string = "stanger") {
  return `Hi there,${person}`;
}

//애너테이션과 파라미터가 붙어있어야 좋음.

인사2(); // stranger
인사2("tony"); // tony
인사2(123); // 오류

//
// 리턴 타입 애너테이션
//

function 사각형3(num: number): number {
  return num * num;
}

function 랜덤(num: number) {
  // iDE 에서 이 함수는 반환값이 "문자열"이거나 "숫자 " 일것이라고 인지함.
  if (Math.random() < 0.5) {
    return num.toString();
  }
  return num;
}

const 덧셈 = (x: number, y: number): number => {
  // 리턴타입을 파라미터 뒤에 적어주면 됨.
  return x + y;
};

//
// 익명 함수 문맥상(Context) 타입 지정
//

//자바스크립트는
//하나의 함수를 forEach/map 같은 다른함수에 전달함

const 색 = ["빨강", "노랑", "파랑", "오렌지"]; //JS 배열
//e는 string이라고 알아서 ts에서 잡아줌.
색.map((e) => {
  return e.toUpperCase();
  // return e.toFixed(); // 오류 출력
}); //  이런 경우 애너테이션을 작성 안 해두 됨.

function 엥(color) {} // color 의 타입은 any

//
// Void 타입
//

// void는 변수가아닌 함수에 주로 작성함
// 아무것도 반환하지 않는 함수의 반환 타입으로 사용

function 복붙(msg: string): void {
  console.log(msg);
  console.log(msg);
  return ""; // void 명시후 return작성시 에러
}

// 이 함수는 return이 없어서 void 에너테이션을 갖고 있음.

//
// Never 타입
//
// TS에 고유 타입
// 함수의 반환 타입과 // 반환하면 안되는 함수에 주로 쓰임

// void와 차이;
// void는 아무것도 반환하지 않은 경우, 함수의 백그라운드에서 undefined 상태로 반환
// Never는 함수가 아무것도 반환하면 안된다고 명시

function 에러만들기(msg: string): never {
  throw new Error(msg);
}

function 게임반복(): never {
  while (true) {
    console.log("게임반복 진행중");
  }
  return true; // 오류
}

//void는 엄밀히 말하자면 return 값 : undefined를 리턴함
//never : 함수가 절대 반환을 하면 안됨.
