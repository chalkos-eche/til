//
//배열 타입 다루기
//

const 활성유저: [] = [];
활성유저.push("이씨"); //에러뜸 타입[]은 빈배열이란 뜻.

// 문자열타입 배열
const 활성유저2: string[] = [];

활성유저2.push("김씨");
활성유저2.push(123); //에러 뜸

const 나이리스트: number[] = [4, 5, 23, 4];
나이리스트[0] = 4;
나이리스트[1] = "응애"; //에러뜸

//
// 배열 구문 더 알아보기
//

const 불: Array<boolean> = [];
const 불2: boolean[] = [];
//위에 두개다 같은 의미.

// 커스텀 타입으로도 선언할 수 있다.

type Point = {
  x: number;
  y: number;
};

const 좌표: Point[] = [];
좌표.push({ x: 40, y: 8 });
좌표.push({ x: 40, y: "8" }); //숫자가아니라 에러
좌표.push({ x: 8 }); // y값이 없어서 에러뜸

//
// 다차원 배열
//

const board: string[] = [
  ["X", "O", "x"],
  ["X", "O", "x"],
  ["X", "O", "x"],
];

//다차원 배열에서는 배열개수를 같이 타입 지정해준다
//위에는 2차원배열인데 배열 개수가 []1개라 에러

const board2: string[][] = [
  ["X", "O", "x"],
  ["X", "O", "x"],
  ["X", "O", "x"],
];

// 3차원배열
const demo: number[][][] = [[[1]]];
