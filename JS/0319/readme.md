#### `0319`

> ## Promise
>
> ### 프라미스를 활용한 비동기 처리 예시
>
> - 일반
>
> ````js
> function getTodo(){
> let todo;
> $.get('http://도메인.com/api', function(response) {
> todo = response;
> return todo;
> });
> }
> // get 을 통해 api 를 가져와 todo 함수에 데이터를 저장한다. todo를 반환하는 함수
> // 결과물은 undefined ( 응답 값을 정상적으로 확인 X
> > ```
> * **프라미스**
> ```js
> function getTodo() {
> return new Promise((res,rej)=> {
>   $.get('http://도메인.com/api',()=> {
>     resolve(response);
>     });
>   });
> getTodo.then();
> ````

> ## Async & Await
>
> ### 기본 문법
>
> - 함수앞에 async 를 붙여주고 내부 비동기 앞에 await 를 붙여줌
>   ( 정확하게: promise 객체 반환 앞에 await를 붙임)
>
> ```js
> // 기본 문법
> async function fetchdata() {
>   await getUserList();
> }
> ```
>
> ### 기본 예제
>
> ```js
> async function fetchdata() {
>   var list = await getUserList(); // 프라미스를 반환하는 함수
>   console.log(list);
> }
> function getUserList() {
>   return new Promise(function (res, rej) {
>     var userList = ["user1", "user2", "user3"];
>     resolve(userList);
>   });
> }
> // fetchdata(); // ['user1','user2','user3']
> ```
>
> ### 정리
>
> 비동기 함수(프라미스)가 성공하고 난 후, 비동기 성공 시 .then 을 사용
>
> ```js
> delayedColorChange('red',1000);
> .then(()=> delayedColorChange('orange',1000))
> .then(()=> delayedColorChange('yellow',1000))
> .then(()=> delayedColorChange('green',1000))
> .then(()=> delayedColorChange('blue',1000))
> .then(()=> delayedColorChange('indigo',1000))
> .then(()=> delayedColorChange('violet',1000))
> ```
>
> async 로 바꾸기
>
> ```js
> async function rainbow() {
>   await delayedColorChange("red", 1000);
>   //await 를 사용하면 Promise 가 결과를 낼 때 까지 기다림
>   await delayedColorChange("orange", 1000);
>   await delayedColorChange("yellow", 1000);
>   await delayedColorChange("green", 1000);
>   await delayedColorChange("blue", 1000);
>   await delayedColorChange("indigo", 1000);
>   await delayedColorChange("violet", 1000);
>   return "all Done";
> }
> // rainbow 함수 실행후 문구가 출력되도록 할 수 있음
> rainbow().then(() => "end of rainbow");
> ```
>
> new Promise return 인 함수에게 앞에다 await 붙이고,  
> 함수앞에 async 를 붙여 비동기 화 할 수 있다.  
> 완료후 .then 으로 완료후(성공후) 작동할 수 있다.

- 예제)

```js
const delay = 4000;
const 통신요청함수 = (URL) => {
  return new Promise((res, rej) => {
    setTimeout(() => {
      res("데이터 성공");
      rej("에러");
    }, delay);
  });
};

async function 두개의요청함수() {
  // let data1 = await 통신요청함수('/URL');
  try {
    // try문에 오류가 뜰 수 있는 함수를 적으면
    let data1 = await 통신요청함수("URL/1");
    let data2 = await 통신요청함수("URL/2");
  } catch (e) {
    // catch문에서 어떻게 처리할지 명령문을 작성 ( e는 에러 )
    console.log("에러감지");
    console.log("에러감지:", e); // 매개변수 : 에러
  }
}
```
