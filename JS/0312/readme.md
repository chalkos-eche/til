####

#### `0312`

# Vue 들어가기전 ES6 훑고가기

> ## PROMISE
>
> ~~몇번을봐도 모르겠어..~~
>
> - 쁘라미스를 사용해야 하는 이유 :
>
> 1. **콜백 지옥**의 해결
> 2. **에러**처리의 용이성
>    > ### 1. 콜백 지옥
>    >
>    > 예제 : 비동기적으로 처리해야할 작업이 둘 이상을 가정한 코드 ;
>    >
>    > ```
>    > // loadScript 라는 임의의 함수
>    > function loadScript(src,callback) {
>    > //파라미터 자바스크립트 주소 // 콜백 함수
>    > let script =document.createElement('script');
>    > // script 태그 생성
>    > script.src = src;
>    > // script태그의 속성은 자바스크립트의 주소라 할당.
>    > script.onload() = () => callback(script);
>    > 스크립트가 실행시 스크립트를 매개변수로하는 콜백함수를 실행 (?);
>    > // 여기서 콜백함수는 스크립트 로드가 끝나면 실행된다.
>    > // onload 메서드로 callback을 실행
>    > i.e) 일단 src의 script를 일차적으로 실행 -> 실행이 완료되면 콜백함수를 실행
>    > document.head.append(script);
>    > //
>    > //script 태그 DOM head에 입력.
>    > //
>    > //
>    > loadScript('1.js', function(error, script) {
>    >    if (error) {
>    >        handleError(error);
>    >    } else {
>    >        // ...
>    >        loadScript('2.js', function(error, script) {
>    >            if (error) {
>    >                handleError(error);
>    >            } else {
>    >                // ...
>    >                loadScript('3.js', function(error, script) {
>    >                    if (error) {
>    >                        handleError(error);
>    >                    } else {
>    >                        // ...continue after all scripts are loaded (*)
>    >                    }
>    >                });
>    >            }
>    >        });
>    >    }
>    > });
>    >
>    > ```
>    >
>    > 1. 1.js를 호출 (에러뜨면 handleError작동)
>    > 2. 정상적이면 2.js를 호출 ( //)
>    > 3. 정상적이면 3.js를 호출 (//)
>    > 4. ... 반복
>    >    코드의 가독성이 안좋아져 **프로미스**를 사용하는것.
>
> > ### 2. 에러 처리
> >
> > - 사실 콜백 지옥은 프라미스를 활용하지 않고, 해결할 수 있다.
> > - **익명함수의 사용을 포기**, **콜백함수를 분리** 하면 된다
> >
> > ```
> > loadScript('1.js', step1);
> > // 콜백함수를 정의
> > function step1(error, script) {
> > if (error) {
> > handleError(error);
> > } else {
> > // ..
> > loadScript('2.js', step2);
> > }
> > }
> >
> > // 두번쨰 콜백함수를 정의
> > function step2(error, script) {
> > if (error) {
> > handleError(error);
> > } else {
> > // ..
> > loadScript('3.js', step3);
> > }
> > }
> > ```
>
> - 이런 방법보다 **프로미스**를 이용하는 이유는 **에러처리가 쉽다**
> - 프로미스를 통한 에러처리는 **.then .catch** 메소드를 이용한다.
> - 프로미스를 이용한 콜백 대처 예제 ;
>
> ```
> function loadScript(src) {
> return new Promise((resolve, reject) => {
> const script = document.createElement('script');
>        script.src = src;
>
>        script.onload = () => resolve(script);
>        script.onerror = () => reject(new Error('Error!'));
>
>       document.head.append(script);
>    });
> }
>
> loadScript('callback.js')
> .then(console.log)
> .catch(console.log);
>
> ```
>
> ### 기본 문법
>
> - const promise = new Promise((resolve, reject) => { //함수(executor)}
> - new Promise () 에 전달되는 함수는 실행함수(executor)로, 프로미스 생성후 자동적으로 실행됨.
> - 프로미스의 객체는 두가지 속성 state, result 두가지
> -
