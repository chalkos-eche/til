#### `입문`

`https://joshua1988.github.io/vue-camp/textbook.html`

> ## ES6+ (자바스크립트 복습)
>
> ### \* Enhanced Object Literal ( 향상된 객체 리터럴 )
>
> ---
>
> > (기존 객체 정의 방식)
> >
> > ```
> > let 강아지 = {
> > name : 'LEO',
> > breed  : 'Bichon',
> > speak : function () {
> > console.log('bark!');
> > ```
> >
> > **(축약문법 1)**
> >
> > ```
> > let 언어 = "자바스크립트 ";
> > let 나 = { 언어 };
> > console.log(나) // 언어:"자바스크립트";
> > ```
> >
> > **속성과 값이 같으면 1개만 기입**
>
> - **축약문법을 Vue 적용하기**
>   > ```
>   > // #1 - 컴포넌트 등록 방식에서의 축약 문법
>   > const myComponent = {
>   > template: '<p>My Component</p>'
>   > };
>   > new Vue({
>   > components: {
>   > // myComponent: myComponent
>   > myComponent
>   > }
>   > });
>   > // #2 - 라우터 등록 방식에서의 축약 문법
>   > const router = new VueRouter({
>   > // ...
>   > });
>   >
>   > new Vue({
>   > // router: router,
>   > router
>   > });
>   > ```
>   >
>   > ### 속성함수 예약어 생략
>   >
>   > (기존방식)
>   >
>   > ```
>   > const 예제객체 = {
>   > // 속성: 함수
>   > 함수: function() {
>   > console.log('Hello World')
>   > }
>   > 예제객체.함수(); // Hello World
>   > ```
>   >
>   > **(축약문법2)**
>   >
>   > ```
>   > const 예제객체 ={
>   > 함수() {
>   > console.log('hello,world');
>   >    }
>   > };
>   > 예제객체.함수(); // hello,world
>   > ```
>   >
>   > - **축약문법2를 Vue 적용하기**
>   >
>   > ```
>   > new Vue({
>   > methods:{
>   > fetchData(){} // fetchData : function() {}
>   > showAlert(){} // showAlert : function() {}
>   >    }
>   >  });
>   > ```

> ### \* Spread Operator(스프레드 오퍼레이터)
>
> ---
>
> > **특정 객체, 배열 복제,이동** : [...] 이런 모습
> >
> > **사용법;**
> >
> > ```
> > const 객체 = { a:10,b:20,};
> > const 새 객체 = {...객체}; // 얕은 복사
> >
> > const 배열 = [1,2,3];
> > const 뉴배열 = [...배열]; // 얕은 복사
> > ```
> >
> > 기존 자바스크립트 객체 복제 방식
> >
> > ```
> > const obj ={a:10,b:20,}
> > const newObj ={a:obj.a,b:obj.b}
> >
> > const arr = [1,2,3];
> > const newArr = [arr[0],arr[1],arr[2]]
> > ```
>
> - Spread Operator **Vuex 에 적용하기**  
>   (가장 많이 사용하는 부분은 **Vuex의 헬퍼 함수**)
>
> ```
> import { mapGetters} form 'vuex';
> export default {
> computed:{
>     ...mapGetters(['getStrings','getNumbers','getUsers']),
>      anothercounter() {...}
>       }
>     }
> ```

> ### \* Destructuring(구조 분해 문법)
