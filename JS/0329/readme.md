#### `0329-30`

> ## Query 상태의 이해
>
> - ":" 은 변수를 나타냄 /:id {{id}} <id> ( id로 대체하라 )
> - ?sort=desc&color=blue
> - 쿼리문자열엔 여러가지가 들어갈 수 있다.
> - 어떤 키값 쌍으로 가능

> ## 헤더
>
> - 헤더는 요청과 함꼐 정보를 전달하는 부수적인 방식으로서 응답에도 포함
> -

> ## XMLHttpRequest
>
> - 옛날 방식 / 변천사를 알기위해 배워야함
> - as XHR
> - 프로미스를 지원하지않아 어마어마한 콜백 요청
> - **HTTP 로 보통쓰는데 여긴 Http..임..**
>
> ```js
> const req = new XMLHttpRequest();
> req.onload = function () {
> const data =JSON.parse(this.reponseText);
> console.log(data);
> }; // 오류가 없을 때
> req.onerror = function () {}; // 오류가 있을 때
> req.open("GET", "https://swapi.dev/api/people/1/"); // api 열기
> req.send; // 전송
> // https://swapi.dev/api/people/1/
> ```
> * ## FETCH 때문에 안써두댐 안외워두댐 아디오스


> ## FETCH!!! 나온지 10년됐지만
> ```js
> fetch("https://swapi.dev/api/people/1/") // 프로미스를 얻음
> .then(res =>{
> console.log('resolved',res);
> return res.json();
> }).then((data)=>{
> console.log('json done',data);
> return fetch("https://swapi.dev/api/people/2/")
> })
> //.then(res =>{return res.json()}
> .catch(e =>{
> console.log('error',e)
> })
> // fetch 호출시 프로미스를 얻어옴, 처리되면 resolved 출력 / 에러뜨면 error
> // response 는 생긴게 당혹스러움
> // fetch는 헤더를 호출 하자마자 프로미스가 resolve(처리)되기 때문
> //  즉 , 본문 전체가 보이지 않음 JSON 프로미스가  처리되기전에 만들어져서 보이지않음 
> // 그럴땐 .json 메소드를 사용해야함
> // 1. fetch 로 URL 요청을 보냄
> // then 콜백에 도달(프로미스가 처리) resolved 출력
> // 그다음에 res.json() 을 반환
> // 그다음에 then 콜백으로 res.json 을 console로 확인
> // 2페이지를 출력하고싶으면 1페이지 json 까지 가져온후 
> // return 값으로 2페이지 fetch를 한다 
> // 그리고 .then을 적어 이어가면 됨..
> 
> ```
> ### 비동기 함수를 이용해 리팩터링 하기
> ```js
> try { 
> const 함수 = async () => {
> const res= await fetch("https://swapi.dev/api/people/1/") // promise 를 가져오니 await 를 해준다.
> const data = await res.json() //fetch가 끝난후 작업해야하니 await 를 달아준다
> const res2= await fetch("https://swapi.dev/api/people/2/") 
> const data2 = await res2.json() //
> } .catch (e) {
> console.log("error", e);
> }
> 
> ```
> * json 파싱을 해야하는게 불편하고 아직 덜개선되었다.


> ## 완전체 AXIOS;
> * 라이브러리,
> ```js
> axios.get("https://swapi.dev/api/people/1/"); // axios.get 할시 promise가 return 됨.
> .then(result =>{
> console.log(result) // fetch의 json 작업을 안해도 됨.
>})
> .catch(e =>{console.log(e)}
> 
> ```
> ### 비동기함수로 axios 작성하기
> ```js
> try {
> const 비동기함수 = async(id) => {
> const res = await axios.get(`https://swapi.dev/api/people/${id}/`); await 로 promise를 기다린다.
> res.data // json 객체를 바로 확인 할 수 있다.
> } catch (e) {
> }
> 비동기함수(5); 
> 비동기함수(10); 
> ```

> ## AXIOS로 헤더 세팅하기
> ```js
> const 함수 =  async()=>{
> const res = await axios.get("https://swapi.dev/api/people/1/");
> const config = {headers:{Aceept:'application/json',},}
> } // axios 가 자동으로 파싱 
> ```
